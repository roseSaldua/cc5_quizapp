//
//  File.swift
//  Quizzler
//
//  Created by ronne on 11/08/2019.
//  Copyright © 2019 London App Brewery. All rights reserved.
//

import Foundation


class Question {
    
    let questionText: String
    let corAnswer: Bool
    
    init( text: String, correctAnswer: Bool){
        
        questionText = text
        corAnswer = correctAnswer
        
    }
    
    
}


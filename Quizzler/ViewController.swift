//
//  ViewController.swift
//  Quizzler
//
//  Created by Angela Yu on 25/08/2015.
//  Copyright (c) 2015 London App Brewery. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    
    var questions = ListOfQuestions()
    var questionNumber = 0
    var pickedAnswer: Bool  = true
    var score = 0
    
    
    
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet var progressBar: UIView!
    @IBOutlet weak var progressLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
 
        nextQuestion()
    }


    @IBAction func answerPressed(_ sender: AnyObject) {
        
        
        if (sender.tag == 1) {
            pickedAnswer = true
        } else {
            pickedAnswer = false
        }
        
        checkAnswer()
        
        questionNumber += 1
        
        nextQuestion()
        
    }
    
    
    func updateUI() {
        
        scoreLabel.text = "Score: \(score)"
        progressLabel.text = "\(questionNumber + 1)/13"
        progressBar.frame.size.width = (view.frame.size.width / 13) * CGFloat (questionNumber + 1)
        
      
    }
    

    func nextQuestion() {

        
        if questionNumber <= 12 {
            questionLabel.text = questions.list[questionNumber].questionText
            updateUI()
            
        } else {
            
           let alert = UIAlertController.init(title: "Congratulations", message: "You've finished the game! Let's start over again.", preferredStyle: .alert)
            let restartAction = UIAlertAction.init(title: "Restart", style: .default) { (UIAlertAction) in
                self.startOver()
            
            }
            
            alert.addAction(restartAction)
            
            present(alert, animated: true, completion: nil)
            
        }
        
        
    }
    
    
    func checkAnswer() {
        
        if pickedAnswer == questions.list[questionNumber].corAnswer {
            ProgressHUD.showSuccess("Correct!")
            score += 1
        } else {
            ProgressHUD.showError("Wrong")
        }
        
        
    }
    
    
    func startOver() {
        score = 0
        questionNumber = 0
        nextQuestion()
       
    }
    

    
}
